# Pasos para ejecutar la aplicación

1. Asegurarse de tener instalado los siguientes paquetes:
    - Uvicorn
    - Pydantic
    - FastAPI
    - Pandas
    - Sklearn
    - Joblib 
    - Nltk
    - Langdetect 

2. Descargar el proyecto.
3. Entrar al archivo `etapa2/back-end/funcion_utils.py`.
4. Descomentar de la linea número 31 a la 33 y ejecutar el archivo, esto con el fin de asegurarte de que todo está funcionando correctamente (esto es solo necesario hacerlo una vez, después de eso se deberían volver a comentar para evitar que el archivo intente descargar nuevamente los archivos de NLTK cuando se referencie function_utils.py o que imprima los datos ya verificados mostrando datos innecesarios en la consola).
    - Linea 31: Abrirá el downloader de ntlk. (Seguir instrucciones del paso 5)
    - Linea 32: Verificará el langdetect arrojando el resultado `es` a menos que se cambie la linea 29.
    - Linea 33: Dará un valor cercano a 313 palabras si funciona bien.
5. Una vez en el NLTK Downloader (deberia estar corriendo en terminal): Ingresar el carácter 'd' para descargar de la librería nltk, después 'all' para descargar todos los paquetes y luego de que finalice ingresar el carácter 'q' para salir del downloader de nltk.
6. Abrir la terminal sobre la carpeta `back-end` y ejecutar `uvicorn main:app --reload`. Para asegurarte que todo salió bien, debe salir en consola `Application startup complete`. Además asegurarse que uvicorn este corriendo en el puerto 8000 en el host local `127.0.0.1` (generalmente indicado en la segunda linea que dice `INFO`), esto último es necesario para que la página web haga las peticiones correctamente.
7. Abrir el archivo `etapa2/pagina-web/web.html`. Nota: No usar live server para abrir este archivo ya que puede recargar la página al usar el endpoint de re-entrenamiento ya que sobrescribe el joblib y live server toma esto como una señal de que debe recargar la página, lo que puede llevar a que no funcione correctamente.
