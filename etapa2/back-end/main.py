from typing import Optional
from dataModel import DataModel
from fastapi import FastAPI
import pandas as pd
from joblib import load, dump
from dataModelPred import DataModelPred
from sklearn.metrics import make_scorer, confusion_matrix, classification_report, precision_score, recall_score, f1_score, accuracy_score, ConfusionMatrixDisplay, average_precision_score
import numpy as np
from sklearn.preprocessing import RobustScaler, OrdinalEncoder, OneHotEncoder, PolynomialFeatures, FunctionTransformer
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.compose import ColumnTransformer
from sklearn.impute import KNNImputer, SimpleImputer
from function_utils import pre_pre_processing, text_pre_processing, pre_process_df 
from fastapi.middleware.cors import CORSMiddleware
from typing import List

pipe_line = "pipe.joblib"

app = FastAPI()
origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.post("/predict")
def make_predictions(dataModel: DataModel):
    print(dataModel)
    df = pd.DataFrame(dataModel.dict(), columns=dataModel.dict().keys(), index=[0])
    df.columns = dataModel.columns()
    model = load(pipe_line)
    result = model.predict(df)
    result=result[0]
    return result

@app.post("/multipredict")
def make_multiple_predictions(dataModel: List[DataModel]):
    print(dataModel)
    data=[]
    for dat in dataModel:
        data.append(dat.dict())
    df = pd.DataFrame(data)
    df.columns = dataModel[0].columns()
    model = load(pipe_line)
    array = model.predict(df)
    result = dict(zip(range(len(array)), array))
    return result

@app.post("/train")
def re_train(dataModel: List[DataModelPred]):
    print(dataModel)
    data=[]
    for dat in dataModel:
        data.append(dat.dict())
    df = pd.DataFrame(data)
    df.columns = dataModel[0].columns()
    target="sentimiento"
    cols = list(df.columns)
    cols.remove(target)
    x = df[cols]
    y= df[target]
    model = load(pipe_line)

    model.fit(x,y)
    dump(model, pipe_line)
    report=classification_report(y, model.predict(x), output_dict=True)
    result = pd.DataFrame(report)
    result = result.to_html()
    return result


