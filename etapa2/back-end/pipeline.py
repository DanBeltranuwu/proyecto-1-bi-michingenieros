#Librerias básicas
import numpy as np
import random

#pandas
import pandas as pd

#Pipeline
from sklearn.pipeline import Pipeline, make_pipeline
import joblib
from sklearn.svm import SVC
from sklearn.preprocessing import FunctionTransformer, MinMaxScaler

from function_utils import pre_pre_processing, text_pre_processing, pre_process_df

#Selección de modelos
from sklearn.model_selection import GridSearchCV, train_test_split
#Metricas
from sklearn.metrics import make_scorer, confusion_matrix, classification_report, precision_score, recall_score, f1_score, accuracy_score, ConfusionMatrixDisplay, average_precision_score
from sklearn.feature_extraction.text import TfidfVectorizer
print('pipeline')

enco='UTF-8'
data = pd.read_csv("MovieReviews.csv", encoding = enco,sep=',', header=0, index_col=0) 
df = data.copy()
df = data.copy()
df = pre_pre_processing(df)
columns = list(df.columns)
columns.remove('sentimiento')
x_train, x_test, y_train, y_test = train_test_split(
    df[columns],
    df.sentimiento,
    test_size=0.2,
    random_state=777,
    stratify=df.sentimiento
)

pre_process_transformer = FunctionTransformer(pre_process_df)
pipe = Pipeline([
    ('preprocess', pre_process_transformer),
    ('vectorizer', TfidfVectorizer()),
    ('model', SVC(C=100, degree=2, gamma=0.1))
], verbose=True)

pipe.fit(x_train,y_train)
print(pipe.score(x_test, y_test))
filename = 'pipe.joblib'
joblib.dump(pipe, 'pipe.joblib')
