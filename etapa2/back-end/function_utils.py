#Librerias básicas
import numpy as np
import random

#pandas
import pandas as pd

#Pipeline
from sklearn.pipeline import Pipeline, make_pipeline
import joblib

#Preprocesamiento
from sklearn.preprocessing import FunctionTransformer, MinMaxScaler

#Procesamiento del lenguaje
import nltk
from nltk.corpus import stopwords
import string
from langdetect import detect, DetectorFactory

#Modelos a probar
from sklearn.ensemble import RandomForestClassifier
#from sklearn.naive_bayes import GaussianNB
#Segun lo investigado MultinomialNB suele dar mejores resultados que otros clasisficadores 'naive_bayes' en procesamiento de lenguaje natural
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC


text='Hola mundo te quiero mucho'
DetectorFactory.seed = 0
# nltk.download()
# print(detect(text))
# print(len(stopwords.words('spanish')))

def pre_pre_processing(df):
  DetectorFactory.seed = 0
  df=df.drop_duplicates(subset='review_es')
  df['review_es']=df['review_es'].astype(str)
  df['idioma'] = df['review_es'].apply(detect)
  df = df[(df.idioma == 'es')]
  df = df.drop(['idioma'], axis=1)
  df.sentimiento=df.sentimiento.map({
      'positivo':1,
      'negativo':0
  })
  return df

def text_pre_processing(text):
  #Palabras de parada
  stopw = stopwords.words('spanish')
  #Quitar puntuación
  no_punct = ''.join([c for c in text if c not in string.punctuation])
  #Quitar palabras de parada y poner todo en minuscula
  no_stop = ' '.join([w.lower() for w in no_punct.split() if w.lower() not in stopw])
  return no_stop

def pre_process_df(df):
  column_names=['review_es']
  df['review_es']=df['review_es'].apply(text_pre_processing)
  df = df[df.columns.intersection(column_names)]
  #TfidfVectorizer().fit_transform(df['review_es'])
  return df.review_es
